if($('.inner-page').length){
  removeWidthAndHeight();
} else {
  getWidthAndHeight();
}

$(window).resize(function(){
  arrangeElements();
  if($('.inner-page').length){
    removeWidthAndHeight();
  }else{
    getWidthAndHeight();
  }
});

function getWidthAndHeight(){
  var winWidth = $(window).width();
  var winHeight = $(window).height();
  $('.layout,.full_Image,.texture_overlay_1,.texture_gradient,.homeSkills').css({
    'min-height': winHeight
  });
  arrangeElements();
}

function removeWidthAndHeight(){
  $('.layout,.full_Image,.texture_overlay_1,.texture_gradient').attr('style', 'height:300px');
  $('.suggestion-wheel').attr('style', 'height:300px');
}

function arrangeElements(){
  if($(window).width() >= 768 && $(window).width() <= 991){
    $('#portlet_financial_report .financial_report').detach().appendTo('#portlet_financial_report .portlet__inner');
    $('#portlet_financial_report .financial_report').insertBefore('#portlet_financial_report .portlet__bottom-sticky');
  }
}

reSizeSearchBox();
function reSizeSearchBox(){
  if($('.search-nav__input-wrapper').length){
    var topSearchBox = $('.search-nav__input-wrapper').offset().top+ $('.search-nav__input-wrapper').height();
    $('.search__white-gradient-rectangle').css('top',topSearchBox);
  }
};
$(window).resize(function(){
  reSizeSearchBox();
})

var currWindSize = $(window).width();
if(currWindSize >1300){
  $('.suggestion-wheel__item a').css('font-size','1.7em');
}

function tabCarousel(){
  var tabs = $('#project_TabWrapper .nav-tabs > li'),
  active = tabs.filter('.active'),
  next = active.next('li'),
  toClick = next.length ? next.find('a') : tabs.eq(0).find('a');

  toClick.click();
}

function banner1(){
  $('.homeBanners').removeClass('home_Image2 home_Image3 home_Image4').addClass('home_Image1');
  $('.banner-1').removeClass('hide');
  $('.banner-2, .banner-3, .banner-4').addClass('hide');
  $('.banner-indicators li').removeClass('active');
  $('.banner-indicators li:nth-child(1)').addClass('active');
}
function banner2(){
  $('.homeBanners').removeClass('home_Image1 home_Image3 home_Image4').addClass('home_Image2');
  $('.banner-2').removeClass('hide');
  $('.banner-1, .banner-3, .banner-4').addClass('hide');
  $('.banner-indicators li').removeClass('active');
  $('.banner-indicators li:nth-child(2)').addClass('active');
}
function banner3(){
  $('.homeBanners').removeClass('home_Image1 home_Image2 home_Image4').addClass('home_Image3');
  $('.banner-3').removeClass('hide');
  $('.banner-1, .banner-2, .banner-4').addClass('hide');
  $('.banner-indicators li').removeClass('active');
  $('.banner-indicators li:nth-child(3)').addClass('active');
}
function banner4(){
  $('.homeBanners').removeClass('home_Image1 home_Image2 home_Image3').addClass('home_Image4');
  $('.banner-4').removeClass('hide');
  $('.banner-1, .banner-2, .banner-3').addClass('hide');
  $('.banner-indicators li').removeClass('active');
  $('.banner-indicators li:nth-child(4)').addClass('active');
}

var banner = '';
var bannerCount = 1;
function homeBanners(){
  banner = setInterval(function(){
    if(bannerCount == 1) banner1();
    if(bannerCount == 2) banner2();
    if(bannerCount == 3) banner3();
    if(bannerCount == 4){ banner4(); bannerCount = 0;}
    bannerCount++;
  }, 4000);
}

$(document).on('click', '.banner-indicators li', function(e){
  e.preventDefault();

  clearInterval(banner);
  bannerCount = $(this).data('id');
  if(bannerCount == 1) banner1();
  if(bannerCount == 2) banner2();
  if(bannerCount == 3) banner3();
  if(bannerCount == 4){ banner4(); bannerCount = 0;}
  bannerCount++;
});

$('.banner-indicators').hover(function(){
  clearInterval(banner);
}, function(){
  homeBanners();
});

function makeNewPosition(){
  var h = $('.skill_img_wrap').height() - 50;
  var w = $('.skill_img_wrap').width() - 50;
  var nh = Math.floor(Math.random() * h);
  var nw = Math.floor(Math.random() * w);
  var sd = getRandomInt(5000, 15000);
  return [nh, nw, sd];
}

function floatingSkillsIcons(){
  $('.floating-img').each(function(i, e){
    var newq = makeNewPosition();
    $(e).animate({top: newq[0], left: newq[1]}, newq[2], function(){floatingSkillsIcons();});
  })
};

function getRandomInt(min, max){
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

$(document).ready(function(){
  // HOME PAGE Banner
  homeBanners();

  //ALL PAGE Sidebar Menu Section
  $('#sidebar').load('partials/sidebar.html');

  //ALL PAGE Topbar Section
  $('#topbar').load('partials/topbar.html', function(){
    $('.loader').fadeOut('slow');
  });

  //ALL PAGE Menubar Section
  $('#navbar').load('partials/navbar.html', function(){
    $(".dropdown").hover(function(){
      $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
      $(this).toggleClass('open');
    }, function(){
      $('.dropdown-menu', this).stop(true, true).fadeOut("fast");
      $(this).toggleClass('open');
    });

    $(".dropdown").focusin(function(e){
      if($(e.currentTarget).hasClass('open') === false){
        $('.dropdown-menu', this).stop(true, true).fadeIn("fast");
        $(this).toggleClass('open');
      }
    });
  });

  // ALL PAGE Request Quote Buttons
  $('.requestQuote__wrapper').load('partials/request-quote.html');
  $('#quote-modal').load('partials/quote-modal.html');

  //HOME PAGE Skills Section
  $('#skills').load('partials/skills.html', function(){
    floatingSkillsIcons();
  });

  //HOME PAGE Projects Section
  $('#projects').load('partials/projects.html', function(){
    $('.requestQuote__wrapper').load('partials/request-quote.html');

    var autoCarousel = setInterval(tabCarousel, 5000);

    $('.project__tabs').hover(function(){
      clearInterval(autoCarousel);
    }, function() {
      autoCarousel = setInterval(tabCarousel, 5000);
    });
  });

  //ALL PAGE Testimonials Section
  $('#testimonials').load('partials/testimonials.html', function(){
    $('#quote-carousel').carousel({
      interval: 4000,
    });
  });

  //ALL PAGE Footer Section
  $('#footer').load('partials/footer.html');
});
